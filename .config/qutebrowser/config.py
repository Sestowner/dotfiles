config.load_autoconfig()

c.content.autoplay = False
c.content.javascript.enabled = False
c.content.images = False
c.content.default_encoding = 'UTF-8'
c.content.geolocation = False
c.content.tls.certificate_errors = 'ask'
c.content.webrtc_ip_handling_policy = 'disable-non-proxied-udp'
c.content.headers.do_not_track = True
c.content.blocking.enabled = True
c.content.cookies.accept = 'no-3rdparty'
c.content.cookies.store = False
c.content.private_browsing = True
c.scrolling.smooth = True
c.confirm_quit = ['multiple-tabs', 'downloads']
c.tabs.show = 'never'
c.tabs.background = True
c.tabs.favicons.show = 'pinned'
c.statusbar.show = 'never'
c.colors.webpage.preferred_color_scheme = 'auto'
c.window.hide_decoration = True
c.hints.auto_follow = 'never'
c.url.default_page = 'https://sestolab.pp.ua/startpage'
c.url.start_pages = c.url.default_page
c.url.searchengines = {'DEFAULT': c.url.default_page + '?q={}'}
c.fileselect.handler = 'external'
c.fileselect.folder.command = ['fpicker']
c.fileselect.single_file.command = ['fpicker']
c.fileselect.multiple_files.command = ['fpicker']
c.editor.command = ['st', '-c', 'floating', '-e', 'vis', '{}']
c.new_instance_open_target = 'tab-silent'
c.aliases["w!"] = "session-save -p"

config.bind('o', 'spawn --userscript qbmenu')
config.bind('O', 'spawn --userscript qbmenu -t')
config.bind('go', 'spawn --userscript qbmenu {url:pretty}')
config.bind('gO', 'spawn --userscript qbmenu -t -r {url:pretty}')
config.bind('wo', 'spawn --userscript qbmenu -w')
config.bind('wO', 'spawn --userscript qbmenu -w {url:pretty}')
config.bind(';o', 'hint links spawn --userscript qbmenu {hint-url}')
config.bind(';O', 'hint links spawn --userscript qbmenu -t -r {hint-url}')
config.bind('M', 'spawn -v bmark {url} {title}')

config.bind('tb', 'config-cycle statusbar.show always never')
config.bind('tt', 'config-cycle tabs.show always never')
config.bind('tdm', 'config-cycle colors.webpage.darkmode.enabled;; config-cycle content.user_stylesheets "css/darkmode-fix.css" "";; restart')
config.bind('tah', 'config-cycle -p -t -u *://{url:host}/* content.javascript.enabled;; config-cycle -p -t -u *://{url:host}/* content.images ;; reload')
config.bind('tau', 'config-cycle -p -t -u {url} content.javascript.enabled;; config-cycle -p -t -u {url} content.images ;; reload')

config.bind('xs', 'config-source')

config.bind(',M', 'spawn --detach mpvsh --force-window --no-terminal "{url}"')
config.bind(',m', 'hint links spawn --detach mpvsh --force-window --no-terminal "{hint-url}"')
config.bind(',am', 'hint all spawn --detach mpvsh --force-window --no-terminal "{hint-url}"')
config.bind(',i', 'hint images spawn --detach display "{hint-url}"')
config.bind(',V', 'view-source -e')
config.bind(',d', 'hint all download')
config.bind(',D', 'hint -r all download')
config.bind(',R', 'spawn --userscript qbredir')
config.bind(',r', 'spawn --userscript qbrdrview')
config.bind(',e', 'spawn --userscript qbtext')
config.bind(',sf', 'spawn --userscript qbrss')
config.bind(',f', 'spawn --userscript qbft')
config.bind(',F', 'spawn --userscript qbft -t')
config.bind(',Q', 'spawn --detach sh -c "qrencode -o - -- {url} | display"')
config.bind(',q', 'hint links spawn --detach sh -c "qrencode -o - -- {hint-url} | display"')
config.bind(',aq', 'hint all spawn --detach sh -c "qrencode -o - -- {hint-url} | display"')
config.bind(';y', 'hint links userscript qbcbd -c')
config.bind(';Y', 'hint links userscript qbcbd')
config.bind('yy', 'spawn --userscript qbcbd -c')
config.bind('yY', 'spawn --userscript qbcbd')
config.bind('yt', 'spawn --userscript qbcbd title -c')
config.bind('yT', 'spawn --userscript qbcbd title')
config.bind('yd', 'spawn --userscript qbcbd domain -c')
config.bind('yD', 'spawn --userscript qbcbd domain')
config.bind('ym', 'spawn --userscript qbcbd inline -c')
config.bind('yM', 'spawn --userscript qbcbd inline')
config.bind(',ay', 'hint all userscript qbcbd')
config.bind(',O', 'spawn --detach open {url}')
config.bind(',o', 'hint all spawn --detach open {hint-url}')

#password autofill
config.bind('<z><l>', 'spawn --userscript qbpass')
config.bind('<z><u><l>', 'spawn --userscript qbpass u')
config.bind('<z><p><l>', 'spawn --userscript qbpass p')
config.bind('<z><o><l>', 'spawn --userscript qbpass o')

config.source('solarizedark.py')

