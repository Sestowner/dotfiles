export PATH="$PATH:$HOME/.local/bin"
export XDG_RUNTIME_DIR="/tmp/$(id -u)-runtime-dir"
export EDITOR="vis"
export RCLONE_PASSWORD_COMMAND="pash rclone"
export XDG_CONFIG_HOME="$HOME/.config"
export TMPDIR="/tmp"
export USER_AGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0"

[ "$SHELL" == "/bin/bash" ] && [ -f ~/.bashrc ] && . ~/.bashrc

[ "$(tty)" == "/dev/tty1" ] && exec ssession

