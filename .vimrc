" Use Vim defaults instead of 100% vi compatibility
" Avoid side-effects when nocompatible has already been set.
if &compatible
	set compatible
endif

set cursorline
set encoding=utf-8
set backspace=indent,eol,start
set matchpairs+=<:>
set list
set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,precedes:«,extends:»
set wildmenu
set showmatch
set hlsearch
set smartcase
set ignorecase
set incsearch
set ruler
set number relativenumber
set smarttab
set autoindent
set shiftwidth=4
set softtabstop=4
set tabstop=4
set scrolloff=4
set modeline
set viminfofile=~/.cache/vim/viminfo
syntax on
filetype plugin indent on
set background=dark
colorscheme solarized
let mapleader="\<Space>"

" Move temporary files to a secure location to protect against CVE-2017-1000382
let &g:directory=$HOME . '/.cache'
let &g:undodir=&g:directory . '/vim/undo//'
let &g:backupdir=&g:directory . '/vim/backup//'
let &g:directory.='/vim/swap//'

" Create directories if they doesn't exist
if ! isdirectory(expand(&g:directory))
	silent! call mkdir(expand(&g:directory), 'p', 0700)
endif
if ! isdirectory(expand(&g:backupdir))
	silent! call mkdir(expand(&g:backupdir), 'p', 0700)
endif
if ! isdirectory(expand(&g:undodir))
	silent! call mkdir(expand(&g:undodir), 'p', 0700)
endif

let g:vimspector_enable_mappings = 'HUMAN'

" Mapping
noremap <Leader>st :execute 'set showtabline=' . (&showtabline ==# 0 ? 2 : 0)<CR>
noremap <Leader>\t :vertical rightbelow terminal<CR>
noremap <Leader>tt :terminal<CR>
noremap <Leader>ta :tab terminal<CR>
noremap <Leader>dt :exec(&diff ? 'diffoff' : 'diffthis')<CR>
noremap <Leader>cb :set invcursorbind<CR>
noremap <Leader>ts :set invspell<CR>
noremap <Leader>cd :if getcwd() == expand('%:p:h') <bar> lcd - <bar> else <bar> lcd %:p:h <bar> endif<CR>
noremap <Leader>th :set invhlsearch<CR>
noremap <Leader>tp :set invpaste<CR>
noremap <Leader>ssf :syntax sync fromstart<CR>
noremap <Leader>di <Plug>VimspectorBalloonEval
xnoremap <Leader>di <Plug>VimspectorBalloonEval
xnoremap <Leader>dr <Plug>VimspectorContinue
noremap <Leader>dx :silent! call vimspector#Reset()<CR>
noremap <Leader>dcb :silent! call vimspector#ClearBreakpoints()<CR>
noremap <Leader>dw :call vimspector#AddWatch(expand("<cexpr>"))<CR>
noremap <Leader>dv :VimspectorShowOutput<Space>
nnoremap <leader>/ :execute ':'.system('fzy -q "'.expand("<cword>").'"<bar>cut -f1', map(getline(1, '$'),'printf("%d\t%s", v:key + 1, v:val)'))<bar>redraw!<cr>
nnoremap <leader>g/ :exec ':e '.system('ag . --silent <bar> fzy <bar> awk -F: '..shellescape('{print "+"$2" "$1}'))<bar>redraw!<cr>
nnoremap <leader>fe :call FzyCommand("find . -type f", ":e")<cr>
nnoremap <leader>fv :call FzyCommand("find . -type f", ":vs")<cr>
nnoremap <leader>fs :call FzyCommand("find . -type f", ":sp")<cr>
nnoremap <leader>ft :call FzyCommand("find . -type f", ":tabe")<cr>


" Colors
hi CursorLineNr cterm=none
hi CursorLine ctermfg=none ctermbg=black cterm=none
hi VertSplit ctermfg=none ctermbg=none cterm=none
hi SpecialKey ctermbg=none
hi NonText ctermfg=darkgray ctermbg=none
hi Folded cterm=none

" Expand or jump
imap <expr> <C-l>   vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'
smap <expr> <C-l>   vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'

" Automatic commands
au BufReadPost * call setpos(".", getpos("'\"")) " Remember cursor position
au BufWritePre * %s/\s\+$//e " Delete all trailing whitespace on save

function! FzyCommand(choice_command, vim_command)
	try
		let output = system(a:choice_command . " -not -path '*.git/*' -not -path '*node_modules/*' | fzy ")
	catch /Vim:Interrupt/
	endtry
	redraw!
	if v:shell_error == 0 && !empty(output)
		exec a:vim_command . ' ' . output
	endif
endfunction

